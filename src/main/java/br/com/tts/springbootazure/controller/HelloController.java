package br.com.tts.springbootazure.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping(path = "/group1")
    @ResponseBody
    @PreAuthorize("hasRole('diogroup1')")
    public String group1(){
        return "Hello Group 1 Users";
    }

    @GetMapping(path = "/group2")
    @ResponseBody
    @PreAuthorize("hasRole('diogroup2')")
    public String group2(){
        return "Hello Group 2 Users";
    }


    @GetMapping(path = "/group3")
    @ResponseBody
    public String group3(){
        return "Hello Group 3 Users";
    }
}
